const supertest = require("supertest");
const httpStatus = require("http-status");

const app = require("../src/app");
const Merchant = require("../src/models/merchant");
const { generateToken } = require("../src/lib/helpers");
const { isUUID } = require("validator");
const Transaction = require("../src/models/transaction");
const { encrypt } = require("../src/lib/crypto");
const Payout = require("../src/models/payouts");
const request = supertest(app);

const testMerchant = {
  email: "XYZ@xyz.com",
  password: "123456",
  name: "xyz ltd.",
  accountNumber: "2087280977",
  bankCode: "033",
};

const cardTransaction = {
  amount: "15000",
  currency: "NGN",
  description: "For Jollof rice",
  type: "card",
  cardDetails: {
    number: "51994444333322221111",
    cardHolder: "Ilodiuba Victor",
    expiryDate: "06/25",
    cvv: "911",
  },
};

const virtualAccountTransaction = {
  amount: "10000",
  currency: "NGN",
  description: "For Jollof rice",
  type: "virtual_account",
  customerAccountDetails: {
    accountName: "Ilodiuba Victor Nnamdi",
    accountNumber: "2087280977",
    bankCode: "033",
  },
};

const cardTransactionUSD = {
  amount: "5000",
  currency: "USD",
  description: "For Jollof rice",
  type: "card",
  cardDetails: {
    number: "51994444333322221111",
    cardHolder: "Ilodiuba Victor",
    expiryDate: "06/25",
    cvv: "911",
  },
};

const virtualAccountTransactionUSD = {
  amount: "3000",
  currency: "USD",
  description: "For Jollof rice",
  type: "virtual_account",
  customerAccountDetails: {
    accountName: "Ilodiuba Victor Nnamdi",
    accountNumber: "2087280977",
    bankCode: "033",
  },
};

let token;

describe("Transactions", () => {
  beforeAll(async () => {
    const merchant = await Merchant.create(testMerchant);

    testMerchant.id = merchant.id;

    token = generateToken(merchant.id, "1h");

    cardTransaction.merchant_id = merchant.id;
    virtualAccountTransaction.merchant_id = merchant.id;

    cardTransactionUSD.merchant_id = merchant.id;
    virtualAccountTransactionUSD.merchant_id = merchant.id;
  });

  afterAll(async () => {
    await Transaction.destroy({ where: { merchant_id: testMerchant.id } });

    await Merchant.destroy({
      where: {
        email: testMerchant.email.toLowerCase(),
      },
    });

    await Payout.destroy({
      where: {
        merchant_id: testMerchant.id,
      },
    });
  });

  describe("Create Transactions", () => {
    test("Successful ngn card transaction initiallization", async () => {
      const response = await request
        .post("/v1/transactions")
        .send(cardTransaction);

      expect(response.status).toBe(httpStatus.CREATED);
      expect(response.body.message).toBe("Success");
      expect(response.body.data?.transaction).toBeDefined();
      expect(response.body.data.transaction.reference).toBeDefined();
      expect(isUUID(response.body.data.transaction.reference)).toBe(true);
      expect(parseFloat(response.body.data.transaction.amount)).toEqual(
        parseFloat(cardTransaction.amount)
      );
      expect(response.body.data.transaction.currency).toEqual(
        cardTransaction.currency
      );
      expect(response.body.data.transaction.description).toEqual(
        cardTransaction.description
      );
      expect(response.body.data.transaction.status).toEqual("pending");
      expect(response.body.data.transaction.merchant_id).toEqual(
        testMerchant.id
      );
      expect(response.body.data.transaction.cardDetails.number).toEqual(
        cardTransaction.cardDetails.number.slice(-4)
      );
      expect(response.body.data.transaction.cardDetails.cvv).toEqual("***");

      cardTransaction.reference = response.body.data.transaction.reference;

      const transaction = await Transaction.findByPk(
        response.body.data.transaction.reference
      );
      expect(transaction).toBeDefined();
      expect(transaction.reference).toBeDefined();
      expect(isUUID(transaction.reference)).toBe(true);
      expect(parseFloat(transaction.amount)).toEqual(
        parseFloat(cardTransaction.amount)
      );
      expect(transaction.currency).toEqual(cardTransaction.currency);
      expect(transaction.description).toEqual(cardTransaction.description);
      expect(transaction.channels).toEqual(cardTransaction.channels);
      expect(transaction.customerEmail).toEqual(cardTransaction.customerEmail);
      expect(transaction.status).toEqual("pending");
      expect(transaction.merchant_id).toEqual(testMerchant.id);
      expect(transaction.cardDetails.number).toEqual(
        cardTransaction.cardDetails.number.slice(-4)
      );
      expect(transaction.cardDetails.cvv).toEqual(
        encrypt(cardTransaction.cardDetails.cvv)
      );
    });

    test("Successful usd card transaction initiallization", async () => {
      const response = await request
        .post("/v1/transactions")
        .send(cardTransactionUSD);

      expect(response.status).toBe(httpStatus.CREATED);
      expect(response.body.message).toBe("Success");
      expect(response.body.data?.transaction).toBeDefined();
      expect(response.body.data.transaction.reference).toBeDefined();
      expect(isUUID(response.body.data.transaction.reference)).toBe(true);
      expect(parseFloat(response.body.data.transaction.amount)).toEqual(
        parseFloat(cardTransactionUSD.amount)
      );
      expect(response.body.data.transaction.currency).toEqual(
        cardTransactionUSD.currency
      );
      expect(response.body.data.transaction.description).toEqual(
        cardTransactionUSD.description
      );
      expect(response.body.data.transaction.status).toEqual("pending");
      expect(response.body.data.transaction.merchant_id).toEqual(
        testMerchant.id
      );
      expect(response.body.data.transaction.cardDetails.number).toEqual(
        cardTransactionUSD.cardDetails.number.slice(-4)
      );
      expect(response.body.data.transaction.cardDetails.cvv).toEqual("***");

      cardTransactionUSD.reference = response.body.data.transaction.reference;

      const transaction = await Transaction.findByPk(
        response.body.data.transaction.reference
      );
      expect(transaction).toBeDefined();
      expect(transaction.reference).toBeDefined();
      expect(isUUID(transaction.reference)).toBe(true);
      expect(parseFloat(transaction.amount)).toEqual(
        parseFloat(cardTransactionUSD.amount)
      );
      expect(transaction.currency).toEqual(cardTransactionUSD.currency);
      expect(transaction.description).toEqual(cardTransactionUSD.description);
      expect(transaction.channels).toEqual(cardTransactionUSD.channels);
      expect(transaction.customerEmail).toEqual(
        cardTransactionUSD.customerEmail
      );
      expect(transaction.status).toEqual("pending");
      expect(transaction.merchant_id).toEqual(testMerchant.id);
      expect(transaction.cardDetails.number).toEqual(
        cardTransactionUSD.cardDetails.number.slice(-4)
      );
      expect(transaction.cardDetails.cvv).toEqual(
        encrypt(cardTransactionUSD.cardDetails.cvv)
      );
    });

    test("Successful virtual account initiallization", async () => {
      const response = await request
        .post("/v1/transactions")
        .send(virtualAccountTransaction);

      expect(response.status).toBe(httpStatus.CREATED);
      expect(response.body.message).toBe("Success");
      expect(response.body.data?.transaction).toBeDefined();
      expect(response.body.data.transaction.reference).toBeDefined();
      expect(isUUID(response.body.data.transaction.reference)).toBe(true);
      expect(parseFloat(response.body.data.transaction.amount)).toEqual(
        parseFloat(virtualAccountTransaction.amount)
      );
      expect(response.body.data.transaction.currency).toEqual(
        virtualAccountTransaction.currency
      );
      expect(response.body.data.transaction.description).toEqual(
        virtualAccountTransaction.description
      );
      expect(response.body.data.transaction.status).toEqual("success");
      expect(response.body.data.transaction.merchant_id).toEqual(
        testMerchant.id
      );

      virtualAccountTransaction.reference =
        response.body.data.transaction.reference;

      const transaction = await Transaction.findByPk(
        response.body.data.transaction.reference
      );
      expect(transaction).toBeDefined();
      expect(transaction.reference).toBeDefined();
      expect(isUUID(transaction.reference)).toBe(true);
      expect(parseFloat(transaction.amount)).toEqual(
        parseFloat(virtualAccountTransaction.amount)
      );
      expect(transaction.currency).toEqual(virtualAccountTransaction.currency);
      expect(transaction.description).toEqual(
        virtualAccountTransaction.description
      );
      expect(transaction.channels).toEqual(virtualAccountTransaction.channels);
      expect(transaction.customerEmail).toEqual(
        virtualAccountTransaction.customerEmail
      );
      expect(transaction.status).toEqual("success");
      expect(transaction.merchant_id).toEqual(testMerchant.id);
    });

    test("Successful usd  virtual account initiallization", async () => {
      const response = await request
        .post("/v1/transactions")
        .send(virtualAccountTransactionUSD);

      expect(response.status).toBe(httpStatus.CREATED);
      expect(response.body.message).toBe("Success");
      expect(response.body.data?.transaction).toBeDefined();
      expect(response.body.data.transaction.reference).toBeDefined();
      expect(isUUID(response.body.data.transaction.reference)).toBe(true);
      expect(parseFloat(response.body.data.transaction.amount)).toEqual(
        parseFloat(virtualAccountTransactionUSD.amount)
      );
      expect(response.body.data.transaction.currency).toEqual(
        virtualAccountTransactionUSD.currency
      );
      expect(response.body.data.transaction.description).toEqual(
        virtualAccountTransactionUSD.description
      );
      expect(response.body.data.transaction.status).toEqual("success");
      expect(response.body.data.transaction.merchant_id).toEqual(
        testMerchant.id
      );

      virtualAccountTransactionUSD.reference =
        response.body.data.transaction.reference;

      const transaction = await Transaction.findByPk(
        response.body.data.transaction.reference
      );
      expect(transaction).toBeDefined();
      expect(transaction.reference).toBeDefined();
      expect(isUUID(transaction.reference)).toBe(true);
      expect(parseFloat(transaction.amount)).toEqual(
        parseFloat(virtualAccountTransactionUSD.amount)
      );
      expect(transaction.currency).toEqual(
        virtualAccountTransactionUSD.currency
      );
      expect(transaction.description).toEqual(
        virtualAccountTransactionUSD.description
      );
      expect(transaction.channels).toEqual(
        virtualAccountTransactionUSD.channels
      );
      expect(transaction.customerEmail).toEqual(
        virtualAccountTransactionUSD.customerEmail
      );
      expect(transaction.status).toEqual("success");
      expect(transaction.merchant_id).toEqual(testMerchant.id);
    });
  });

  describe("Get Transaction", () => {
    test("Successful get card transaction", async () => {
      const response = await request.get(
        `/v1/transactions/${cardTransaction.reference}`
      );

      expect(response.status).toBe(httpStatus.OK);
      expect(response.body.message).toBe("Success");
      expect(response.body.data?.transaction).toBeDefined();
      expect(response.body.data.transaction.reference).toBeDefined();
      expect(isUUID(response.body.data.transaction.reference)).toBe(true);
      expect(parseFloat(response.body.data.transaction.amount)).toEqual(
        parseFloat(cardTransaction.amount)
      );
      expect(response.body.data.transaction.currency).toEqual(
        cardTransaction.currency
      );
      expect(response.body.data.transaction.description).toEqual(
        cardTransaction.description
      );
      expect(response.body.data.transaction.status).toEqual("pending");
      expect(response.body.data.transaction.merchant_id).toEqual(
        testMerchant.id
      );
    });

    test("Successful get virtual account transaction", async () => {
      const response = await request.get(
        `/v1/transactions/${virtualAccountTransaction.reference}`
      );

      expect(response.status).toBe(httpStatus.OK);
      expect(response.body.message).toBe("Success");
      expect(response.body.data?.transaction).toBeDefined();
      expect(response.body.data.transaction.reference).toBeDefined();
      expect(isUUID(response.body.data.transaction.reference)).toBe(true);
      expect(parseFloat(response.body.data.transaction.amount)).toEqual(
        parseFloat(virtualAccountTransaction.amount)
      );
      expect(response.body.data.transaction.currency).toEqual(
        virtualAccountTransaction.currency
      );
      expect(response.body.data.transaction.description).toEqual(
        virtualAccountTransaction.description
      );
      expect(response.body.data.transaction.status).toEqual("success");
      expect(response.body.data.transaction.merchant_id).toEqual(
        testMerchant.id
      );
    });
  });

  describe("Settle Card Payments", () => {
    test("Successful Settlements", async () => {
      const response = await request.post("/v1/transactions/settle").send({
        reference: cardTransaction.reference,
        amount: cardTransaction.amount,
        cardNumber: cardTransaction.cardDetails.number,
        currency: cardTransaction.currency,
      });

      expect(response.status).toBe(httpStatus.CREATED);
      expect(response.body.message).toBe("Success");
      expect(response.body.data?.transaction).toBeDefined();
      expect(response.body.data.transaction.reference).toBeDefined();
      expect(isUUID(response.body.data.transaction.reference)).toBe(true);
      expect(parseFloat(response.body.data.transaction.amount)).toEqual(
        parseFloat(cardTransaction.amount)
      );
      expect(response.body.data.transaction.currency).toEqual(
        cardTransaction.currency
      );
      expect(response.body.data.transaction.description).toEqual(
        cardTransaction.description
      );
      expect(response.body.data.transaction.status).toEqual("success");
      expect(response.body.data.transaction.merchant_id).toEqual(
        testMerchant.id
      );
    });
  });

  describe("Get Balance", () => {
    test("Get merchant's balance", async () => {
      const response = await request
        .get("/v1/transactions/balance")
        .set({ Authorization: `Bearer ${token}` });

      expect(response.status).toBe(httpStatus.OK);
      expect(response.body.data?.balances).toBeDefined();
      expect(response.body.data.balances).toEqual({
        available: {
          USD: "2850.00",
          NGN: "24050.00",
        },
        pending_settlements: {
          USD: "4850.00",
          NGN: "0",
        },
      });
    });
  });

  describe("Get Transactions", () => {
    beforeAll(async () => {
      for (let index = 0; index < 10; index++) {
        await request.post("/v1/transactions").send(virtualAccountTransaction);
      }
    });

    test("Pagination", async () => {
      const response = await request
        .get("/v1/transactions")
        .set({ Authorization: `Bearer ${token}` });

      expect(response.status).toBe(httpStatus.OK);
      expect(response.body.meta).toBeDefined();
      expect(response.body.data?.transactions).toBeDefined();
      expect(Array.isArray(response.body.data?.transactions)).toBe(true);
      expect(response.body.meta).toEqual({
        totalTransactions: 14,
        page: 1,
        pageSize: 10,
        totalPages: 2,
        previousPage: null,
        nextPage: 2,
      });
    });

    test("Pagination with specific page", async () => {
      const response = await request
        .get("/v1/transactions?page=2")
        .set({ Authorization: `Bearer ${token}` });

      expect(response.status).toBe(httpStatus.OK);
      expect(response.body.meta).toBeDefined();
      expect(response.body.data?.transactions).toBeDefined();
      expect(Array.isArray(response.body.data?.transactions)).toBe(true);
      expect(response.body.meta).toEqual({
        totalTransactions: 14,
        page: 2,
        pageSize: 4,
        totalPages: 2,
        previousPage: 1,
        nextPage: null,
      });
    });

    test("Pagination with status filter", async () => {
      const response = await request
        .get("/v1/transactions?status=pending")
        .set({ Authorization: `Bearer ${token}` });

      expect(response.status).toBe(httpStatus.OK);
      expect(response.body.meta).toBeDefined();
      expect(response.body.data?.transactions).toBeDefined();
      expect(Array.isArray(response.body.data?.transactions)).toBe(true);
      expect(
        response.body.data?.transactions.every(
          (transaction) => transaction.status === "pending"
        )
      ).toBe(true);
      expect(response.body.meta).toEqual({
        totalTransactions: 1,
        page: 1,
        pageSize: 1,
        totalPages: 1,
        previousPage: null,
        nextPage: null,
      });
    });

    test("Pagination with currency filter", async () => {
      const response = await request
        .get("/v1/transactions?page=2&currency=NGN")
        .set({ Authorization: `Bearer ${token}` });

      expect(response.status).toBe(httpStatus.OK);
      expect(response.body.meta).toBeDefined();
      expect(response.body.data?.transactions).toBeDefined();
      expect(Array.isArray(response.body.data?.transactions)).toBe(true);
      expect(
        response.body.data?.transactions.every(
          (transaction) => transaction.currency === "NGN"
        )
      ).toBe(true);
      expect(response.body.meta).toEqual({
        totalTransactions: 12,
        page: 2,
        pageSize: 2,
        totalPages: 2,
        previousPage: 1,
        nextPage: null,
      });
    });

    test("Pagination with greater than amount filter", async () => {
      const response = await request
        .get("/v1/transactions?page=1&gteAmount=15000&currency=NGN")
        .set({ Authorization: `Bearer ${token}` });

      expect(response.status).toBe(httpStatus.OK);
      expect(response.body.meta).toBeDefined();
      expect(response.body.data?.transactions).toBeDefined();
      expect(Array.isArray(response.body.data?.transactions)).toBe(true);
      expect(
        response.body.data?.transactions.every(
          (transaction) =>
            parseFloat(transaction.amount) >= 15000 &&
            transaction.currency === "NGN"
        )
      ).toBe(true);
      expect(response.body.meta).toEqual({
        totalTransactions: 1,
        page: 1,
        pageSize: 1,
        totalPages: 1,
        previousPage: null,
        nextPage: null,
      });
    });

    test("Pagination with greater than amount filter", async () => {
      const response = await request
        .get("/v1/transactions?page=2&lteAmount=14999.99&currency=NGN")
        .set({ Authorization: `Bearer ${token}` });

      expect(response.status).toBe(httpStatus.OK);
      expect(response.body.meta).toBeDefined();
      expect(response.body.data?.transactions).toBeDefined();
      expect(Array.isArray(response.body.data?.transactions)).toBe(true);
      expect(
        response.body.data?.transactions.every(
          (transaction) =>
            parseFloat(transaction.amount) <= 14999.99 &&
            transaction.currency === "NGN"
        )
      ).toBe(true);
      expect(response.body.meta).toEqual({
        totalTransactions: 11,
        page: 2,
        pageSize: 1,
        totalPages: 2,
        previousPage: 1,
        nextPage: null,
      });
    });
  });
});
