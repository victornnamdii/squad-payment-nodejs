const supertest = require("supertest");
const httpStatus = require("http-status");

const app = require("../src/app");
const Merchant = require("../src/models/merchant");
const { generateToken, comparePassword } = require("../src/lib/helpers");
const { isUUID } = require("validator");
const request = supertest(app);

const testMerchant = {
  email: "XYZ@xyz.com",
  password: "123456",
  name: "xyz ltd.",
  accountNumber: "2087280977",
  bankCode: "033",
};

let token;

describe("Merchants", () => {
  afterAll(async () => {
    await Merchant.destroy({
      where: {
        email: testMerchant.email.toLowerCase(),
      },
    });

    await Merchant.destroy({
      where: {
        email: "xyz@yyy.com",
      },
    });
  });

  describe("Sign up", () => {
    test("Successful sign up", async () => {
      const response = await request.post("/v1/merchant").send(testMerchant);

      expect(response.status).toBe(httpStatus.CREATED);
      expect(response.body.message).toBe("Success");
      expect(response.body.data?.merchant).toBeDefined();
      expect(response.body.data?.merchant.id).toBeDefined();
      expect(response.body.data.merchant.accountName).toBeDefined();
      expect(response.body.data.merchant.password).toBeUndefined();
      expect(response.body.data.merchant.email).toEqual(
        testMerchant.email.toLowerCase()
      );
      expect(response.body.data.merchant.name).toEqual(
        testMerchant.name.toUpperCase()
      );
      expect(response.body.data.merchant.accountNumber).toEqual(
        testMerchant.accountNumber
      );
      expect(response.body.data.merchant.bankCode).toEqual(
        testMerchant.bankCode
      );

      testMerchant.id = response.body.data.merchant.id;

      const merchant = await Merchant.findByPk(response.body.data.merchant.id);
      expect(merchant).toBeDefined();
      expect(merchant.id).toBeDefined();
      expect(merchant.accountName).toBeDefined();
      expect(merchant.email).toEqual(testMerchant.email.toLowerCase());
      expect(merchant.name).toEqual(testMerchant.name.toUpperCase());
      expect(merchant.accountNumber).toEqual(testMerchant.accountNumber);
      expect(merchant.bankCode).toEqual(testMerchant.bankCode);

      // Check if password was hashed
      expect(merchant.password).not.toEqual(testMerchant.password);
      expect(comparePassword(testMerchant.password, merchant.password)).toEqual(
        true
      );
    });

    test("Duplicate Email", async () => {
      const response = await request.post("/v1/merchant").send({
        email: "XYZ@xYz.com",
        password: "123456",
        name: "mtn ltd.",
        accountNumber: "2087280977",
        bankCode: "033",
      });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe(
        `Merchant with email '${testMerchant.email.toLowerCase()}' already exists`
      );
      expect(response.body.data?.merchant).toBeUndefined();
    });

    test("Invalid Email", async () => {
      let response = await request.post("/v1/merchant").send({
        email: "XYZ",
        password: "123456",
        name: "mtn ltd.",
        accountNumber: "2087280977",
        bankCode: "033",
      });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe("Please enter a valid email");
      expect(response.body.data?.merchant).toBeUndefined();

      response = await request.post("/v1/merchant").send({
        password: "123456",
        name: "mtn ltd.",
        accountNumber: "2087280977",
        bankCode: "033",
      });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe("email is required");
      expect(response.body.data?.merchant).toBeUndefined();

      response = await request.post("/v1/merchant").send({
        email: ["xyz@xyz.com"],
        password: "123456",
        name: "mtn ltd.",
        accountNumber: "2087280977",
        bankCode: "033",
      });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe(
        "email cannot be an array or an object"
      );
      expect(response.body.data?.merchant).toBeUndefined();

      const merchant = await Merchant.findOne({
        where: { email: "xyz" },
      });
      expect(merchant).toBe(null);
    });

    test("Duplicate Email", async () => {
      const response = await request.post("/v1/merchant").send({
        email: "XYZ@xYz.com",
        password: "123456",
        name: "mtn ltd.",
        accountNumber: "2087280977",
        bankCode: "033",
      });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe(
        `Merchant with email '${testMerchant.email.toLowerCase()}' already exists`
      );
      expect(response.body.data?.merchant).toBeUndefined();

    });

    test("Invalid Password", async () => {
      let response = await request.post("/v1/merchant").send({
        email: "XYZ@yyy.com",
        password: "12345",
        name: "mtn ltd.",
        accountNumber: "2087280977",
        bankCode: "033",
      });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe(
        "Your password should be at least 6 characters"
      );
      expect(response.body.data?.merchant).toBeUndefined();

      response = await request.post("/v1/merchant").send({
        email: "XYZ@yyy.com",
        name: "mtn ltd.",
        accountNumber: "2087280977",
        bankCode: "033",
      });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe("password is required");
      expect(response.body.data?.merchant).toBeUndefined();

      response = await request.post("/v1/merchant").send({
        email: "xyz@yyy.com",
        password: ["123456"],
        name: "mtn ltd.",
        accountNumber: "2087280977",
        bankCode: "033",
      });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe(
        "password cannot be an array or an object"
      );
      expect(response.body.data?.merchant).toBeUndefined();

      const merchant = await Merchant.findOne({
        where: { email: "xyz@yyy.com" },
      });
      expect(merchant).toBe(null);
    });

    test("Invalid Name", async () => {
      let response = await request.post("/v1/merchant").send({
        email: "XYZ@yyy.com",
        password: "123456",
        name: "mtn",
        accountNumber: "2087280977",
        bankCode: "033",
      });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe(
        "Your business name should be at least 4 characters"
      );
      expect(response.body.data?.merchant).toBeUndefined();

      response = await request.post("/v1/merchant").send({
        email: "XYZ@yyy.com",
        password: "123456",
        accountNumber: "2087280977",
        bankCode: "033",
      });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe("name is required");
      expect(response.body.data?.merchant).toBeUndefined();

      response = await request.post("/v1/merchant").send({
        email: "xyz@yyy.com",
        password: "123456",
        name: ["mtn ltd."],
        accountNumber: "2087280977",
        bankCode: "033",
      });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe(
        "name cannot be an array or an object"
      );
      expect(response.body.data?.merchant).toBeUndefined();

      const merchant = await Merchant.findOne({
        where: { email: "xyz@yyy.com" },
      });
      expect(merchant).toBe(null);
    });

    test("Invalid Account Number", async () => {
      let response = await request.post("/v1/merchant").send({
        email: "XYZ@yyy.com",
        password: "123456",
        name: "mtn ltd.",
        accountNumber: "208728097",
        bankCode: "033",
      });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe(
        "Account number should be a string containing a 10 digit number"
      );
      expect(response.body.data?.merchant).toBeUndefined();

      response = await request.post("/v1/merchant").send({
        email: "XYZ@yyy.com",
        password: "123456",
        name: "mtn ltd",
        bankCode: "033",
      });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe("accountNumber is required");
      expect(response.body.data?.merchant).toBeUndefined();

      response = await request.post("/v1/merchant").send({
        email: "xyz@yyy.com",
        password: "123456",
        name: "mtn ltd.",
        accountNumber: ["2087280977"],
        bankCode: "033",
      });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe(
        "accountNumber cannot be an array or an object"
      );
      expect(response.body.data?.merchant).toBeUndefined();

      response = await request.post("/v1/merchant").send({
        email: "XYZ@yyy.com",
        password: "123456",
        name: "mtn ltd.",
        accountNumber: "2087280978",
        bankCode: "033",
      });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe(
        "Could not resolve account name. Check parameters or try again."
      );
      expect(response.body.data?.merchant).toBeUndefined();

      const merchant = await Merchant.findOne({
        where: { email: "xyz@yyy.com" },
      });
      expect(merchant).toBe(null);
    });

    test("Invalid Bank Code", async () => {
      let response = await request.post("/v1/merchant").send({
        email: "XYZ@yyy.com",
        password: "123456",
        name: "mtn ltd.",
        accountNumber: "2087280977",
        bankCode: "03",
      });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe("Bank code is invalid");
      expect(response.body.data?.merchant).toBeUndefined();

      response = await request.post("/v1/merchant").send({
        email: "XYZ@yyy.com",
        password: "123456",
        name: "mtn ltd",
        accountNumber: "2087280977",
      });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe("bankCode is required");
      expect(response.body.data?.merchant).toBeUndefined();

      response = await request.post("/v1/merchant").send({
        email: "xyz@yyy.com",
        password: "123456",
        name: "mtn ltd.",
        accountNumber: "2087280977",
        bankCode: ["033"],
      });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe(
        "bankCode cannot be an array or an object"
      );
      expect(response.body.data?.merchant).toBeUndefined();

      response = await request.post("/v1/merchant").send({
        email: "XYZ@yyy.com",
        password: "123456",
        name: "mtn ltd.",
        accountNumber: "2087280978",
        bankCode: "777",
      });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe("Bank code is invalid");
      expect(response.body.data?.merchant).toBeUndefined();

      const merchant = await Merchant.findOne({
        where: { email: "xyz@yyy.com" },
      });
      expect(merchant).toBe(null);
    });
  });

  describe("Log in", () => {
    test("Successful log in", async () => {
      const response = await request
        .post("/v1/merchant/login")
        .send(testMerchant);

      expect(response.status).toBe(httpStatus.OK);
      expect(response.body.message).toBe("Success");
      expect(response.body.data?.token).toBeDefined();

      token = response.body.data.token;
    });

    test("Missing Email/Password", async () => {
      let response = await request
        .post("/v1/merchant/login")
        .send({ email: testMerchant.email });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe(
        "Please enter your email and password"
      );
      expect(response.body.data?.token).toBeUndefined();

      response = await request
        .post("/v1/merchant/login")
        .send({ password: testMerchant.password });

      expect(response.status).toBe(httpStatus.BAD_REQUEST);
      expect(response.body.message).toBe(
        "Please enter your email and password"
      );
      expect(response.body.data?.token).toBeUndefined();
    });

    test("Invalid Credentials", async () => {
      let response = await request
        .post("/v1/merchant/login")
        .send({ email: testMerchant.email, password: "wrongpass" });

      expect(response.status).toBe(httpStatus.UNAUTHORIZED);
      expect(response.body.message).toBe("Email or password is incorrect");
      expect(response.body.data?.token).toBeUndefined();

      response = await request
        .post("/v1/merchant/login")
        .send({ email: "xyz@xyz", password: testMerchant.password });

      expect(response.status).toBe(httpStatus.UNAUTHORIZED);
      expect(response.body.message).toBe("Email or password is incorrect");
      expect(response.body.data?.token).toBeUndefined();

      response = await request
        .post("/v1/merchant/login")
        .send({ email: "xyz@xyz", password: ["123456"] });

      expect(response.status).toBe(httpStatus.UNAUTHORIZED);
      expect(response.body.message).toBe("Email or password is incorrect");
      expect(response.body.data?.token).toBeUndefined();

      response = await request
        .post("/v1/merchant/login")
        .send({ email: [testMerchant.email], password: "123456" });

      expect(response.status).toBe(httpStatus.UNAUTHORIZED);
      expect(response.body.message).toBe("Email or password is incorrect");
      expect(response.body.data?.token).toBeUndefined();
    });
  });

  describe("Token Tests", () => {
    test("Get profile", async () => {
      const response = await request
        .get("/v1/merchant")
        .set({ Authorization: `Bearer ${token}` });

      expect(response.status).toBe(httpStatus.OK);
      expect(response.body.message).toBe("Success");
      expect(response.body.data?.merchant).toBeDefined();
      expect(response.body.data.merchant.id).toBeDefined();
      expect(isUUID(response.body.data.merchant.id)).toBe(true);
      expect(response.body.data.merchant.accountName).toBeDefined();
      expect(response.body.data.merchant.password).toBeUndefined();
      expect(response.body.data.merchant.email).toEqual(
        testMerchant.email.toLowerCase()
      );
      expect(response.body.data.merchant.name).toEqual(
        testMerchant.name.toUpperCase()
      );
      expect(response.body.data.merchant.accountNumber).toEqual(
        testMerchant.accountNumber
      );
      expect(response.body.data.merchant.bank).toEqual(testMerchant.bank);
    });

    test("No Bearer Token", async () => {
      let response = await request
        .get("/v1/merchant")
        .set({ Authorization: "JWT" });

      expect(response.status).toBe(httpStatus.UNAUTHORIZED);
      expect(response.body.message).toBe("Invalid Authorization");
      expect(response.body.data?.merchant).toBeUndefined();

      response = await request.get("/v1/merchant");

      expect(response.status).toBe(httpStatus.UNAUTHORIZED);
      expect(response.body.message).toBe("Invalid Authorization");
      expect(response.body.data?.merchant).toBeUndefined();

      response = await request
        .get("/v1/merchant")
        .set({ Authorization: "Bearer " });

      expect(response.status).toBe(httpStatus.UNAUTHORIZED);
      expect(response.body.message).toBe("Invalid Authorization");
      expect(response.body.data?.merchant).toBeUndefined();
    });

    test("Invalid Token", async () => {
      let response = await request
        .get("/v1/merchant")
        .set({ Authorization: "Bearer invalid" });

      expect(response.status).toBe(httpStatus.FORBIDDEN);
      expect(response.body.message).toBe("Forbidden");
      expect(response.body.data?.merchant).toBeUndefined();

      const invalidToken = generateToken("invalidId", "1h");

      response = await request
        .get("/v1/merchant")
        .set({ Authorization: `Bearer ${invalidToken}` });

      expect(response.status).toBe(httpStatus.FORBIDDEN);
      expect(response.body.message).toBe("Forbidden");
      expect(response.body.data?.merchant).toBeUndefined();

      const expiredToken = generateToken(testMerchant.id, "0s");

      response = await request
        .get("/v1/merchant")
        .set({ Authorization: `Bearer ${expiredToken}` });

      expect(response.status).toBe(httpStatus.FORBIDDEN);
      expect(response.body.message).toBe("Forbidden");
      expect(response.body.data?.merchant).toBeUndefined();
    });
  });
});
