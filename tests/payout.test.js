const supertest = require("supertest");
const httpStatus = require("http-status");

const app = require("../src/app");
const Transaction = require("../src/models/transaction");
const Merchant = require("../src/models/merchant");
const Payout = require("../src/models/payouts");
const { generateToken } = require("../src/lib/helpers");
const request = supertest(app);

const testMerchant = {
  email: "XYZ@xyz.com",
  password: "123456",
  name: "xyz ltd.",
  accountNumber: "2087280977",
  bankCode: "033",
};

let token;

describe("Payouts", () => {
  beforeAll(async () => {
    const merchant = await Merchant.create(testMerchant);
    testMerchant.id = merchant.id;

    await Transaction.bulkCreate([
      {
        amount: 15000,
        currency: "NGN",
        description: "For Jollof rice",
        type: "card",
        cardDetails: {
          number: "51994444333322221111",
          cardHolder: "Ilodiuba Victor",
          expiryDate: "06/25",
          cvv: "911",
        },
        fee: 450,
        merchant_id: merchant.id,
        status: "pending",
      },
      {
        amount: 10000,
        currency: "NGN",
        description: "For Jollof rice",
        type: "card",
        cardDetails: {
          number: "51994444333322221111",
          cardHolder: "Ilodiuba Victor",
          expiryDate: "06/25",
          cvv: "911",
        },
        fee: 300,
        merchant_id: merchant.id,
        status: "success",
      },
      {
        amount: 10000,
        currency: "USD",
        description: "For Jollof rice",
        type: "virtual_account",
        customerAccountDetails: {
          accountName: "Ilodiuba Victor Nnamdi",
          accountNumber: "2087280977",
          bankCode: "033",
        },
        fee: 500,
        merchant_id: merchant.id,
        status: "success",
      },
      {
        amount: 5000,
        currency: "USD",
        description: "For Jollof rice",
        type: "virtual_account",
        customerAccountDetails: {
          accountName: "Ilodiuba Victor Nnamdi",
          accountNumber: "2087280977",
          bankCode: "033",
        },
        fee: 250,
        merchant_id: merchant.id,
        status: "success",
      },
    ]);

    token = generateToken(merchant.id, "1h");
  });

  afterAll(async () => {
    await Merchant.destroy({ where: { id: testMerchant.id } });
    await Transaction.destroy({ where: { merchant_id: testMerchant.id } });
    await Payout.destroy({ where: { merchant_id: testMerchant.id } });
  });

  describe("Create Payout", () => {
    test("Successful payout creation", async () => {
      const response = await request
        .post("/v1/payouts")
        .set({ Authorization: `Bearer ${token}` });

      expect(response.status).toBe(httpStatus.CREATED);
      expect(response.body.data?.payouts).toBeDefined();
      expect(Array.isArray(response.body.data.payouts)).toBe(true);
      expect(response.body.data.payouts.length).toBe(2);
      expect(
        response.body.data.payouts.every(
          (payout) =>
            payout.merchant_id === testMerchant.id &&
            ((parseFloat(payout.amount) === 14250 &&
              payout.currency === "USD") ||
              (parseFloat(payout.amount) === 9700 && payout.currency === "NGN"))
        )
      ).toBe(true);
    });
  });
});
