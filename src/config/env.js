require("dotenv").config();

const env = {
  DB_URL: process.env.DB_URL,
  CRYPTO_ALGORITHM: process.env.CRYPTO_ALGORITHM,
  CRYPTO_IV: process.env.CRYPTO_IV,
  CRYPTO_KEY: process.env.CRYPTO_KEY,
  PS_KEY: process.env.PS_KEY,
  PS_URL: process.env.PS_URL,
  SECRET_KEY: process.env.SECRET_KEY,
};

module.exports = env;
