const { Sequelize } = require("sequelize");
const env = require("./env");

const sq = new Sequelize(env.DB_URL, {
  dialect: "postgres",
  logging: false,
});

const connectToDB = async () => {
  await sq.authenticate();
};

module.exports = { sq, connectToDB };
