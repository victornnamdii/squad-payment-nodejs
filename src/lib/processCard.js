const { APIError } = require("./error");
const HttpStatusCode = require("./httpStatusCodes");

const dummyCards = [
  // Unlimited funds (USD)
  {
    number: "51994444333322221111",
    cardHolder: "Ilodiuba Victor",
    expiryDate: "06/25",
    cvv: "911",
    balance: 1000000000,
    currency: "USD",
  },

  // Unlimited funds (NGN)
  {
    number: "51994444333322228975",
    cardHolder: "Ilodiuba Victor",
    expiryDate: "06/25",
    cvv: "951",
    balance: 100000000000,
    currency: "NGN",
  },

  // Limited funds (NGN)

  {
    number: "51995555333322221111",
    cardHolder: "James Wave",
    expiryDate: "05/25",
    cvv: "342",
    balance: 5000,
    currency: "NGN",
  },

  // Limited funds (USD)

  {
    number: "51995555333322561111",
    cardHolder: "James Wave",
    expiryDate: "05/25",
    cvv: "842",
    balance: 50,
    currency: "USD",
  },

  // Always insufficient funds
  {
    number: "51991111222233334444",
    cardHolder: "John Doe",
    expiryDate: "06/25",
    cvv: "372",
    balance: 0,
    currency: "USD",
  },

  // Expired Card
  {
    number: "51997878222233334444",
    cardHolder: "John Emeka",
    expiryDate: "05/23",
    cvv: "942",
    balance: 1000000,
    currency: "NGN",
  },
];

const authenticateCard = (cardDetails) => {
  const existingCard = dummyCards.find((card) => {
    return (
      cardDetails.number.replace(/\s+/g, "") === card.number &&
      cardDetails.cardHolder.toLowerCase() === card.cardHolder.toLowerCase() &&
      cardDetails.expiryDate === card.expiryDate &&
      cardDetails.cvv === card.cvv
    );
  });

  if (existingCard === undefined) {
    throw new APIError(
      "Invalid card details",
      HttpStatusCode.BAD_REQUEST,
      "Card details are invalid. Please enter valid card details"
    );
  }

  const month = existingCard.expiryDate.slice(0, 2);
  const year = existingCard.expiryDate.slice(-2);
  const expiryDate = new Date(`20${year}-${month}-01`);

  if (expiryDate < new Date()) {
    throw new APIError(
      "Expired card",
      HttpStatusCode.BAD_REQUEST,
      "Card has expired. Please enter valid card details"
    );
  }

  return existingCard;
};

const collectFunds = (card, amount, currency) => {
  if (card.currency === currency) {

    if (card.balance < amount) {
      throw new APIError(
        "Insufficient funds",
        HttpStatusCode.BAD_REQUEST,
        "Your card was declined for insufficient funds"
      );
    }
    card.balance -= amount;

  } else if (card.currency === "USD") {

    if (card.balance < amount / 1500) {
      throw new APIError(
        "Insufficient funds",
        HttpStatusCode.BAD_REQUEST,
        "Your card was declined for insufficient funds"
      );
    }
  
    card.balance -= amount / 1500;
  } else if (card.currency === "NGN") {
    // Naira cards are not allowed to make foreign transactions
    throw new APIError(
      "Unauthorized cards",
      HttpStatusCode.BAD_REQUEST,
      "Your card was declined. Please enter valid card details"
    );
  }
};

module.exports = (cardDetails, amount, currency) => {
  const verifiedCard = authenticateCard(cardDetails);

  collectFunds(verifiedCard, amount, currency);
};
