const { isNumeric, isCreditCard, isAlpha } = require("validator");
const banks = require("./banks");
const { APIError } = require("./error");
const HttpStatusCode = require("./httpStatusCodes");

const isCorrectCardDetails = (cardDetails) => {
  if (typeof cardDetails !== "object") {
    throw new APIError(
      "Validation Error",
      HttpStatusCode.BAD_REQUEST,
      "Card details should be an object containing card details"
    );
  }

  const requiredFields = ["number", "cardHolder", "expiryDate", "cvv"];

  requiredFields.forEach((field) => {
    if (cardDetails[field] === undefined) {
      throw new APIError(
        "Validation Error",
        HttpStatusCode.BAD_REQUEST,
        `cardDetails.${field} is required`
      );
    }
  });

  if (
    typeof cardDetails.number !== "string" ||
    !isCreditCard(cardDetails.number)
  ) {
    throw new APIError(
      "Validation Error",
      HttpStatusCode.BAD_REQUEST,
      "Card number should be a string containing only numerical values"
    );
  }

  if (
    typeof cardDetails.cardHolder !== "string" ||
    cardDetails.cardHolder.split(" ").some((name) => !isAlpha(name))
  ) {
    throw new APIError(
      "Validation Error",
      HttpStatusCode.BAD_REQUEST,
      "Card holder name should only contain alphabetic or space characters"
    );
  }

  if (
    typeof cardDetails.expiryDate !== "string" ||
    cardDetails.expiryDate.length !== 5 ||
    !isNumeric(cardDetails.expiryDate.slice(0, 2), { no_symbols: true }) ||
    !isNumeric(cardDetails.expiryDate.slice(-2), { no_symbols: true }) ||
    cardDetails.expiryDate[2] !== "/" ||
    Number(cardDetails.expiryDate.slice(0, 2)) > 12 ||
    Number(cardDetails.expiryDate.slice(0, 2)) < 1
  ) {
    throw new APIError(
      "Validation Error",
      HttpStatusCode.BAD_REQUEST,
      "Card's expiry date for should be in the format MM/YY"
    );
  }

  const month = cardDetails.expiryDate.slice(0, 2);
  const year = cardDetails.expiryDate.slice(-2);
  const expiryDate = new Date(`20${year}-${month}-01`);

  if (expiryDate < new Date()) {
    throw new APIError(
      "Expired card",
      HttpStatusCode.BAD_REQUEST,
      "Card has expired. Please enter valid card details"
    );
  }

  if (
    typeof cardDetails.cvv !== "string" ||
    (cardDetails.cvv.length !== 3 && cardDetails.cvv.length !== 4) ||
    !isNumeric(cardDetails.cvv, { no_symbols: true })
  ) {
    throw new APIError(
      "Validation Error",
      HttpStatusCode.BAD_REQUEST,
      "CVV should be a string of a 3 or 4 digit number"
    );
  }
};

const isCorrectAccountDetails = (accountDetails) => {
  if (typeof accountDetails !== "object") {
    throw new APIError(
      "Validation Error",
      HttpStatusCode.BAD_REQUEST,
      "Customer account details should be an object containing the account details"
    );
  }

  const requiredFields = ["accountNumber", "accountName", "bankCode"];

  requiredFields.forEach((field) => {
    if (!accountDetails[field]) {
      throw new APIError(
        "Validation Error",
        HttpStatusCode.BAD_REQUEST,
        `accountDetails.${field} is required`
      );
    }
  });

  isAccountNumber(accountDetails.accountNumber);

  if (
    typeof accountDetails.accountName !== "string" ||
    accountDetails.accountName.split(" ").some((name) => !isAlpha(name))
  ) {
    throw new APIError(
      "Validation Error",
      HttpStatusCode.BAD_REQUEST,
      "Customer's account name should be a string containing only alphabetic and space characters"
    );
  }

  isBankCode(accountDetails.bankCode);
};

const isAccountNumber = (accountNumber) => {
  if (
    typeof accountNumber !== "string" ||
    accountNumber.length !== 10 ||
    !isNumeric(accountNumber, { no_symbols: true })
  ) {
    throw new APIError(
      "Validation Error",
      HttpStatusCode.BAD_REQUEST,
      "Account number should be a string containing a 10 digit number"
    );
  }
};

const isBankCode = (bankCode) => {
  if (typeof bankCode !== "string" || !banks[bankCode]) {
    throw new APIError(
      "Validation Error",
      HttpStatusCode.BAD_REQUEST,
      "Bank code is invalid"
    );
  }
};

const countDecimals = (number) => {
  const text = number.toString();

  if (text.indexOf("e-") !== -1) {
    const [wholeNumber, RHS] = text.split("e-");
    let decimalPlaces = parseInt(RHS, 10);
    if (decimalPlaces > 0) {
      if (wholeNumber.indexOf(".") !== -1) {
        decimalPlaces += wholeNumber.split(".")[1].length;
      }
      return decimalPlaces;
    }
    throw new Error("Unsupported Number");
  }

  if (Math.floor(number) !== number) {
    return text.split(".")[1]?.length || 0;
  }

  return 0;
};

const isMonetaryValue = (amount) => {
  if (amount === undefined) {
    throw new APIError(
      "Invalid amount",
      HttpStatusCode.BAD_REQUEST,
      "Please specify an amount"
    );
  }

  if (typeof amount !== "number" || amount <= 0 || countDecimals(amount) > 2) {
    throw new APIError(
      "Invalid amount",
      HttpStatusCode.BAD_REQUEST,
      "Amount should be a number greater than 0 with a maximum of 2 decimal places"
    );
  }

  if (amount > 9999999) {
    throw new APIError(
      "Excess amount",
      HttpStatusCode.BAD_REQUEST,
      "Transaction amounts are limited to a maximum of 9,999,999"
    );
  }
};

const isCorrectChannels = (value) => {
  if (
    !Array.isArray(value) ||
    value.some((channel) => !["card", "virtual_account"].includes(channel))
  ) {
    throw new APIError(
      "Validation Error",
      HttpStatusCode.BAD_REQUEST,
      "channels should be an array containing either 'card', 'virtual_account', or both"
    );
  }
};

module.exports = {
  isCorrectCardDetails,
  isCorrectAccountDetails,
  isAccountNumber,
  isBankCode,
  isMonetaryValue,
  isCorrectChannels,
};
