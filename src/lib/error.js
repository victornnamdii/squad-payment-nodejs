const HttpStatusCode = require("./httpStatusCodes");

class BaseError extends Error {
  constructor(name, httpCode, description, isOperational) {
    super(description);
    Object.setPrototypeOf(this, new.target.prototype);

    this.name = name;
    this.httpCode = httpCode;
    this.description = description;
    this.isOperational = isOperational;

    Error.captureStackTrace(this);
  }
}

class APIError extends BaseError {
  constructor(
    name,
    httpCode = HttpStatusCode.INTERNAL_SERVER,
    description = "Internal Server Error",
    isOperational = true
  ) {
    super(name, httpCode, description, isOperational);
  }
}

module.exports = { APIError };
