const { default: axios } = require("axios");
const { hashSync, genSaltSync, compareSync } = require("bcrypt");
const jwt = require("jsonwebtoken");
const env = require("../config/env");
const { APIError } = require("./error");
const HttpStatusCode = require("./httpStatusCodes");
const { Op } = require("sequelize");

const hashString = (string) => {
  const salt = genSaltSync(10);
  return hashSync(string, salt);
};

const comparePassword = (password, hash) => {
  return compareSync(password, hash);
};

const generateToken = (userId, expiresIn) => {
  return jwt.sign({ id: userId }, env.SECRET_KEY, { expiresIn: expiresIn });
};

const decodeToken = (token) => {
  try {
    const decodedToken = jwt.verify(token, env.SECRET_KEY);

    return decodedToken;
  } catch (error) {
    if (
      error instanceof jwt.JsonWebTokenError ||
      error instanceof jwt.NotBeforeError ||
      error instanceof jwt.TokenExpiredError
    ) {
      throw new APIError(
        "Invalid Token",
        HttpStatusCode.FORBIDDEN,
        "Forbidden"
      );
    }
    throw error;
  }
};

const getAccountName = async (accountNumber, bankCode) => {
  try {
    const { data } = await axios.get(env.PS_URL, {
      headers: {
        Authorization: `Bearer ${env.PS_KEY}`,
      },
      params: {
        account_number: accountNumber,
        bank_code: bankCode,
      },
    });

    return data.data.account_name;
  } catch (error) {
    if (error.response?.data?.message) {
      throw new APIError(
        "Bank Account Verification",
        HttpStatusCode.BAD_REQUEST,
        error.response.data.message
      );
    }

    throw error;
  }
};

const roundToTwoDecimalPlaces = (number) => {
  return Math.ceil(number * 100) / 100;
};

const processFilters = (
  merchantId,
  { status, currency, type, gteAmount, lteAmount, payout_id }
) => {
  if (!currency && (gteAmount || lteAmount)) {
    throw new APIError(
      "Incomplete filter",
      HttpStatusCode.BAD_REQUEST,
      "Please specify the currency for the amount filter"
    );
  }

  const query = { merchant_id: merchantId };

  Object.entries({ status, currency, type, payout_id }).forEach(
    ([key, value]) => {
      if (value) {
        query[key] = value;
      }
    }
  );
  if (gteAmount) {
    query.amount = { [Op.gte]: parseFloat(gteAmount) };
  }
  if (lteAmount) {
    query.amount = { [Op.lte]: parseFloat(lteAmount) };
  }

  return query;
};

module.exports = {
  hashString,
  comparePassword,
  generateToken,
  decodeToken,
  getAccountName,
  roundToTwoDecimalPlaces,
  processFilters,
};
