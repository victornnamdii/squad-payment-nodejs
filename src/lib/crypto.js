const crypto = require("crypto");
const env = require("../config/env");

const algorithm = env.CRYPTO_ALGORITHM;
const IV = env.CRYPTO_IV;
const crypto_key = env.CRYPTO_KEY;

class CryptoClient {
  constructor() {
    this.cipher = crypto.createCipheriv(algorithm, crypto_key, IV);
    this.decipher = crypto.createDecipheriv(algorithm, crypto_key, IV);
  }

  encrypt(message) {
    let encryptedMessage = this.cipher.update(
      encodeURI(message),
      "utf-8",
      "hex"
    );
    encryptedMessage += this.cipher.final("hex");

    return encryptedMessage;
  }

  decrypt(data) {
    let decryptedMessage = this.decipher.update(data, "hex", "utf-8");
    decryptedMessage += this.decipher.final("utf-8");

    return decodeURI(decryptedMessage);
  }
}

const encrypt = (string) => {
  const cryptoClient = new CryptoClient();
  return cryptoClient.encrypt(string);
};

const decrypt = (string) => {
  const cryptoClient = new CryptoClient();
  return cryptoClient.decrypt(string);
};

module.exports = { encrypt, decrypt };
