const { UniqueConstraintError, ValidationError } = require("sequelize");
const HttpStatusCode = require("./httpStatusCodes");

const errorHandler = (error) => {
  if (error instanceof UniqueConstraintError) {
    const errItem = error.errors[0];
    return {
      status: HttpStatusCode.BAD_REQUEST,
      message: `Merchant with ${errItem.path} '${errItem.value}' already exists`,
    };
  }

  if (error instanceof ValidationError) {
    const errItem = error.errors[0];
    if (errItem.type.toLowerCase() === "notnull violation") {
      return {
        status: HttpStatusCode.BAD_REQUEST,
        message: `${errItem.path} is required`,
      };
    }

    return { status: HttpStatusCode.BAD_REQUEST, message: errItem.message };
  }

  if (error.name === "SequelizeDatabaseError") {
    const errorMessage = error.original.message;
    if (errorMessage.includes("enum")) {
      if (errorMessage.includes("currency")) {
        return {
          status: HttpStatusCode.BAD_REQUEST,
          message: "currency should be either 'USD' or 'NGN'",
        };
      }
    }
  }

  console.log(error);

  return {
    status: HttpStatusCode.INTERNAL_SERVER,
    message: "Internal Server Error",
  };
};

module.exports = errorHandler;
