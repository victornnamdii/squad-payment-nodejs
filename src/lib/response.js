class ResponseDTO {
  constructor(success, message, data, meta) {
    this.success = success;
    this.message = message || "Success";
    this.meta = meta;
    this.data = data;
  }
}

module.exports = ResponseDTO;
