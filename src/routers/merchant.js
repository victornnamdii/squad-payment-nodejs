const express = require("express");
const MerchantController = require("../controllers/merchantController");
const verifyToken = require("../middlewares/authMiddleware");

const router = express.Router();

router.post("/", MerchantController.signUp);
router.post("/login", MerchantController.logIn);
router.get("/", verifyToken, MerchantController.profile);

module.exports = router;
