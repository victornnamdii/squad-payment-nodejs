const express = require("express");
const PayoutController = require("../controllers/payoutController");
const verifyToken = require("../middlewares/authMiddleware");
const TransactionController = require("../controllers/transactionController");

const router = express.Router();

router.use(verifyToken);
router.post("/", PayoutController.createPayout);
router.get(
  "/:payout_id/transactions",
  TransactionController.getPayoutTransactions
);
router.get("/:payout_id", PayoutController.getPayout);

module.exports = router;
