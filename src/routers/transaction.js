const express = require("express");
const TransactionController = require("../controllers/transactionController");
const verifyToken = require("../middlewares/authMiddleware");

const router = express.Router();

router.post("/", TransactionController.createTransaction);
router.get("/", verifyToken, TransactionController.getAllTransactions);
router.get("/balance", verifyToken, TransactionController.getBalance);
router.post("/settle", TransactionController.settleCardTransaction);
router.get("/:reference", TransactionController.getTransaction);

module.exports = router;
