const { DataTypes } = require("sequelize");
const { sq } = require("../config/db");
const { isMonetaryValue } = require("../lib/customValidators");

const Payout = sq.define("Payout", {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    defaultValue: DataTypes.UUIDV4,
  },
  merchant_id: {
    type: DataTypes.UUID,
    allowNull: false,
  },
  amount: {
    type: DataTypes.DECIMAL(10, 2),
    allowNull: false,
    validate: {
      isMonetaryValue,
    },
  },
  currency: {
    type: DataTypes.ENUM("NGN", "USD"),
    allowNull: false,
    validate: {
      isIn: {
        args: [["NGN", "USD"]],
        msg: "currency should be either 'USD' or 'NGN'",
      },
    },
  },
});

Payout.sync({ alter: true });

module.exports = Payout;
