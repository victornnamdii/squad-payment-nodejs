const { DataTypes } = require("sequelize");
const { sq } = require("../config/db");
const { isAccountNumber, isBankCode } = require("../lib/customValidators");
const {
  hashString,
  getAccountName,
  comparePassword,
  generateToken,
} = require("../lib/helpers");
const { isEmail } = require("validator");
const { APIError } = require("../lib/error");
const HttpStatusCode = require("../lib/httpStatusCodes");

const Merchant = sq.define(
  "Merchant",
  {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    email: {
      type: DataTypes.STRING(100),
      allowNull: false,
      unique: {
        name: "User Email",
        msg: "Email already exists",
      },
      validate: {
        notEmpty: {
          msg: "Please enter a valid email",
        },
        isEmail: {
          msg: "Please enter a valid email",
        },
      },
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: "Please enter your desired password",
        },
        len: {
          args: [6],
          msg: "Your password should be at least 6 characters",
        },
      },
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notEmpty: {
          msg: "Please enter your business name",
        },
        len: {
          args: [4],
          msg: "Your business name should be at least 4 characters",
        },
      },
    },
    accountNumber: {
      type: DataTypes.STRING(10),
      allowNull: false,
      validate: {
        isAccountNumber,
      },
    },
    bankCode: {
      type: DataTypes.STRING(3),
      allowNull: false,
      validate: {
        isBankCode,
      },
    },
    accountName: {
      type: DataTypes.STRING(100),
    },
  },
  {
    hooks: {
      afterValidate: async (instance) => {
        if (instance.password) {
          instance.password = hashString(instance.password);
        }

        if (instance.accountNumber && instance.bankCode) {
          instance.accountName = await getAccountName(
            instance.accountNumber,
            instance.bankCode
          );
        }

        if (instance.email) {
          instance.email = instance.email.toLowerCase();
        }

        if (instance.name) {
          instance.name = instance.name.toUpperCase();
        }
      },
    },
  }
);

Merchant.login = async (email, password) => {
  if (!email || !password) {
    throw new APIError(
      "Missing Credentials",
      HttpStatusCode.BAD_REQUEST,
      "Please enter your email and password"
    );
  }

  if (
    typeof email === "string" &&
    isEmail(email) &&
    typeof password === "string"
  ) {
    const merchant = await Merchant.findOne({
      where: { email: email.toLowerCase() },
    });

    if (merchant !== undefined) {
      const authenticated = comparePassword(password, merchant.password);
      if (authenticated) {
        return generateToken(merchant.id, "1h");
      }
    }
  }

  throw new APIError(
    "Invalid Credentials",
    HttpStatusCode.UNAUTHORIZED,
    "Email or password is incorrect"
  );
};

Merchant.sync({ alter: true });

module.exports = Merchant;
