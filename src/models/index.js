const Merchant = require("./merchant");
const Payout = require("./payouts");
const Transaction = require("./transaction");

// Define associations
Merchant.hasMany(Transaction, { foreignKey: "merchant_id" });
Transaction.belongsTo(Merchant, { foreignKey: "merchant_id" });

Merchant.hasMany(Payout, { foreignKey: "merchant_id" });
Payout.belongsTo(Merchant, { foreignKey: "merchant_id" });

Payout.hasMany(Transaction, { foreignKey: "payout_id" });
Transaction.belongsTo(Payout, { foreignKey: "payout_id" });
