const { DataTypes } = require("sequelize");
const { sq } = require("../config/db");
const {
  isCorrectCardDetails,
  isCorrectAccountDetails,
  isMonetaryValue,
} = require("../lib/customValidators");
const { encrypt } = require("../lib/crypto");
const { APIError } = require("../lib/error");
const HttpStatusCode = require("../lib/httpStatusCodes");
const processCard = require("../lib/processCard");

const Transaction = sq.define(
  "Transaction",
  {
    reference: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    amount: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
      validate: {
        isMonetaryValue,
      },
    },
    currency: {
      type: DataTypes.ENUM("NGN", "USD"),
      allowNull: false,
      validate: {
        isIn: {
          args: [["NGN", "USD"]],
          msg: "currency should be either 'USD' or 'NGN'",
        },
      },
    },
    description: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        len: {
          args: [0, 50],
          msg: "Description should be a maximum of 50 characters",
        },
      },
    },
    type: {
      type: DataTypes.ENUM("card", "virtual_account"),
      allowNull: false,
      validate: {
        isIn: {
          args: [["card", "virtual_account"]],
          msg: "type should be either 'card' or 'virtual_accounts'",
        },
      },
    },
    status: {
      type: DataTypes.ENUM("pending", "success"),
      allowNull: false,
    },
    cardDetails: {
      type: DataTypes.JSONB,
      validate: {
        isCorrectCardDetails,
      },
    },
    customerAccountDetails: {
      type: DataTypes.JSONB,
      validate: {
        isCorrectAccountDetails,
      },
    },
    merchant_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    fee: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
      validate: {
        isMonetaryValue,
      },
    },
    paid_out: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    payout_id: {
      type: DataTypes.UUID
    }
  },
  {
    hooks: {
      beforeCreate: (instance) => {
        if (instance.type === "card") {
          if (!instance.cardDetails) {
            throw new APIError(
              "Missing required fields",
              HttpStatusCode.BAD_REQUEST,
              "Card transactions should contain card details"
            );
          }
          if (instance.customerAccountDetails) {
            throw new APIError(
              "Missing required fields",
              HttpStatusCode.BAD_REQUEST,
              "Card transactions should not contain customer account details"
            );
          }

          processCard(instance.cardDetails, instance.amount, instance.currency);

          instance.cardDetails.number = instance.cardDetails.number.slice(-4);
          instance.cardDetails.cvv = encrypt(instance.cardDetails.cvv);
        }
        if (
          instance.type === "virtual_account" &&
          !instance.customerAccountDetails
        ) {
          if (!instance.customerAccountDetails) {
            throw new APIError(
              "Missing required fields",
              HttpStatusCode.BAD_REQUEST,
              "Virtual account transactions should contain customer account details"
            );
          }
          if (instance.cardDetails) {
            throw new APIError(
              "Missing required fields",
              HttpStatusCode.BAD_REQUEST,
              "Virtual account transactions should not contain card details"
            );
          }
        }
      },
    },
  }
);

Transaction.sync({ alter: true });

module.exports = Transaction;
