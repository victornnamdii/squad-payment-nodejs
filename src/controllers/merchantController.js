const ResponseDTO = require("../lib/response");
const Merchant = require("../models/merchant");

class MerchantController {
  static async signUp(req, res, next) {
    try {
      const { email, password, name, accountNumber, bankCode } = req.body;

      const merchant = await Merchant.create({
        email,
        password,
        name,
        accountNumber,
        bankCode,
      });

      merchant.password = undefined;

      res.status(201).json(new ResponseDTO(true, "", { merchant }));
    } catch (error) {
      next(error);
    }
  }

  static async logIn(req, res, next) {
    try {
      const { email, password } = req.body;

      const token = await Merchant.login(email, password);

      res.status(200).json(new ResponseDTO(true, "", { token }));
    } catch (error) {
      next(error);
    }
  }

  static async profile(req, res, next) {
    try {
      res.status(200).json(new ResponseDTO(true, "", { merchant: req.user }));
    } catch (error) {
      next(error);
    }
  }
}

module.exports = MerchantController;
