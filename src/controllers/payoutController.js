const { literal, Op, Transaction } = require("sequelize");
const { sq } = require("../config/db");
const Payout = require("../models/payouts");
const TransactionModel = require("../models/transaction");
const { APIError } = require("../lib/error");
const HttpStatusCode = require("../lib/httpStatusCodes");
const ResponseDTO = require("../lib/response");
const { isUUID } = require("validator");

class PayoutController {
  static async createPayout(req, res, next) {
    try {
      const merchant_id = req.user.id;
      const trx = await sq.transaction({
        isolationLevel: Transaction.ISOLATION_LEVELS.REPEATABLE_READ,
      });

      let payouts;

      try {
        const lockedRows = await TransactionModel.findAll({
          where: {
            merchant_id,
            paid_out: false,
            status: "success",
          },
          transaction: trx,
          lock: trx.LOCK.UPDATE,
        });

        if (lockedRows.length < 1) {
          throw new APIError(
            "No payouts ready",
            HttpStatusCode.BAD_REQUEST,
            "You have no unpaid settled transactions"
          );
        }

        const unpaidTransactions = await TransactionModel.findOne({
          where: {
            merchant_id,
            paid_out: false,
            status: "success",
          },
          attributes: [
            "merchant_id",
            [
              literal(
                "SUM(CASE WHEN currency = 'USD' THEN amount - fee ELSE 0 END)"
              ),
              "usd_amount",
            ],
            [
              literal(
                "SUM(CASE WHEN currency = 'NGN' THEN amount - fee ELSE 0 END)"
              ),
              "ngn_amount",
            ],
          ],
          group: ["merchant_id"],
          transaction: trx,
        });

        const { ngn_amount, usd_amount } = unpaidTransactions.dataValues;
        const newPayouts = [];
        let usd_payout = false;
        let ngn_payout = false;
        if (parseFloat(ngn_amount) > 0) {
          newPayouts.push({
            merchant_id,
            amount: parseFloat(ngn_amount),
            currency: "NGN",
          });
          ngn_payout = true;
        }
        if (parseFloat(usd_amount) > 0) {
          newPayouts.push({
            merchant_id,
            amount: parseFloat(usd_amount),
            currency: "USD",
          });
          usd_payout = true;
        }

        payouts = await Payout.bulkCreate(newPayouts, {
          transaction: trx,
        });

        let affectedRowsNGN;
        let affectedRowsUSD;

        if (usd_payout) {
          affectedRowsUSD = await TransactionModel.update(
            {
              paid_out: true,
              payout_id: payouts.find((payout) => payout.currency === "USD")
                ?.id,
            },
            {
              where: {
                [Op.or]: lockedRows
                  .filter((row) => row.currency === "USD")
                  .map((transaction) => {
                    return { reference: transaction.reference };
                  }),
                paid_out: false,
                status: "success",
              },
              transaction: trx,
            }
          );
        }

        if (ngn_payout) {
          affectedRowsNGN = await TransactionModel.update(
            {
              paid_out: true,
              payout_id: payouts.find((payout) => payout.currency === "NGN")
                ?.id,
            },
            {
              where: {
                [Op.or]: lockedRows
                  .filter((row) => row.currency === "NGN")
                  .map((transaction) => {
                    return { reference: transaction.reference };
                  }),
                paid_out: false,
                status: "success",
              },
              transaction: trx,
            }
          );
        }

        if (!affectedRowsNGN?.[0] && !affectedRowsUSD?.[0]) {
          throw new APIError(
            "Race Condition",
            HttpStatusCode.BAD_REQUEST,
            "This payout has already been processed"
          );
        }

        await trx.commit();
      } catch (error) {
        await trx.rollback();
        throw error;
      }

      res.status(201).json(new ResponseDTO(true, "", { payouts }));
    } catch (error) {
      next(error);
    }
  }

  static async getPayout(req, res, next) {
    try {
      const { payout_id } = req.params;
      if (!isUUID(payout_id, 4)) {
        throw new APIError(
          "Invalid payout ID",
          HttpStatusCode.BAD_REQUEST,
          "Invalid payout ID"
        );
      }

      const payout = await Payout.findOne({
        where: {
          id: payout_id,
          merchant_id: req.user.id,
        },
      });

      if (payout === null) {
        throw new APIError(
          "Resource not found",
          HttpStatusCode.NOT_FOUND,
          "Payout was not found"
        );
      }

      res.status(200).json(new ResponseDTO(true, "", { payout }));
    } catch (error) {
      next(error);
    }
  }
}

module.exports = PayoutController;
