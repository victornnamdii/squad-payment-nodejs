const { literal } = require("sequelize");
const { APIError } = require("../lib/error");
const { roundToTwoDecimalPlaces, processFilters } = require("../lib/helpers");
const HttpStatusCode = require("../lib/httpStatusCodes");
const ResponseDTO = require("../lib/response");
const Transaction = require("../models/transaction");
const { isUUID } = require("validator");

class TransactionController {
  static async createTransaction(req, res, next) {
    try {
      const {
        amount,
        currency,
        description,
        type,
        cardDetails,
        customerAccountDetails,
        merchant_id,
      } = req.body;

      if (typeof merchant_id !== "string" || !isUUID(merchant_id)) {
        throw new APIError(
          "Invalid merchant id",
          HttpStatusCode.BAD_REQUEST,
          "Invalid merchant id"
        );
      }

      const transaction = await Transaction.create({
        type,
        cardDetails,
        customerAccountDetails,
        amount: parseFloat(amount),
        currency,
        description,
        merchant_id,
        status: type === "card" ? "pending" : "success",
        fee:
          type === "card"
            ? roundToTwoDecimalPlaces((3 * (parseFloat(amount) * 100)) / 10000)
            : roundToTwoDecimalPlaces((5 * (parseFloat(amount) * 100)) / 10000),
      });

      if (type === "card") {
        transaction.cardDetails.cvv = "***";
      }

      res.status(201).json(new ResponseDTO(true, "", { transaction }));
    } catch (error) {
      next(error);
    }
  }

  static async getTransaction(req, res, next) {
    try {
      const { reference } = req.params;
      if (!isUUID(reference)) {
        throw new APIError(
          "Invalid transaction reference",
          HttpStatusCode.BAD_REQUEST,
          "Invalid transaction reference"
        );
      }

      const transaction = await Transaction.findByPk(reference);

      if (transaction === null) {
        throw new APIError(
          "Resource not found",
          HttpStatusCode.NOT_FOUND,
          "Transaction was not found"
        );
      }
      if (transaction.type === "card") {
        transaction.cardDetails.number = "***";
      }

      res.status(200).json(new ResponseDTO(true, "", { transaction }));
    } catch (error) {
      next(error);
    }
  }

  static async getAllTransactions(req, res, next) {
    try {
      const { status, currency, type, gteAmount, lteAmount } = req.query;
      let page = req.query.page;

      // Getting valid page number
      page = Number.parseInt(page, 10);
      if (isNaN(page) || page <= 0) {
        page = 1;
      }

      // Constructing query with provided filters
      const query = processFilters(req.user.id, {
        status,
        currency,
        type,
        gteAmount,
        lteAmount,
      });

      const maxPageSize = 10;
      const { rows, count } = await Transaction.findAndCountAll({
        where: query,
        order: [["createdAt", "DESC"]],
        limit: maxPageSize,
        offset: (page - 1) * maxPageSize,
      });

      const totalPages = Math.ceil(count / maxPageSize);

      res.status(200).json(
        new ResponseDTO(
          true,
          "",
          {
            transactions: rows.map((row) => {
              if (row.type === "card") {
                row.cardDetails.cvv = "***";
              }
              return row;
            }),
          },
          {
            totalTransactions: count,
            page,
            pageSize: rows.length,
            totalPages,
            previousPage:
              page === 1 || totalPages === 0
                ? null
                : page > totalPages
                ? totalPages
                : page - 1,
            nextPage: page < totalPages && totalPages !== 0 ? page + 1 : null,
          }
        )
      );
    } catch (error) {
      next(error);
    }
  }

  static async settleCardTransaction(req, res, next) {
    try {
      const { reference, amount, cardNumber, currency } = req.body;
      if (typeof reference !== "string" || !isUUID(reference)) {
        throw new APIError(
          "Invalid transaction reference",
          HttpStatusCode.BAD_REQUEST,
          "Invalid transaction reference"
        );
      }

      const update = await Transaction.update(
        { status: "success" },
        {
          where: {
            reference,
            type: "card",
            currency,
            amount: parseFloat(amount),
            "cardDetails.number": cardNumber.slice(-4),
            status: "pending",
          },
          returning: true,
        }
      );

      const transaction = update?.[1]?.[0];
      if (!transaction) {
        throw new APIError(
          "Resource not found",
          HttpStatusCode.NOT_FOUND,
          "The details provided do not match any pending card transaction"
        );
      }

      res.status(201).json(new ResponseDTO(true, "", { transaction }));
    } catch (error) {
      next(error);
    }
  }

  static async getBalance(req, res, next) {
    try {
      const merchant_id = req.user.id;

      const balance = await Transaction.findOne({
        where: {
          merchant_id,
        },
        attributes: [
          "merchant_id",
          [
            literal(
              "SUM(CASE WHEN currency = 'USD' \
                AND status = 'success' \
                AND paid_out = false \
                THEN amount - fee ELSE 0 END\
              )"
            ),
            "usd_available_amount",
          ],
          [
            literal(
              "SUM(CASE WHEN currency = 'NGN' \
                AND status = 'success' \
                AND paid_out = false \
                THEN amount - fee ELSE 0 END\
            )"
            ),
            "ngn_available_amount",
          ],
          [
            literal(
              "SUM(CASE WHEN currency = 'USD' \
                AND status = 'pending' \
                AND paid_out = false \
                THEN amount - fee ELSE 0 END\
              )"
            ),
            "usd_pending_settlement",
          ],
          [
            literal(
              "SUM(CASE WHEN currency = 'NGN' \
                AND status = 'pending' \
                AND paid_out = false \
                THEN amount - fee ELSE 0 END\
              )"
            ),
            "ngn_pending_settlement",
          ],
        ],
        group: ["merchant_id"],
      });

      res.status(200).json(
        new ResponseDTO(true, "", {
          balances: {
            available: {
              USD: balance.dataValues.usd_available_amount,
              NGN: balance.dataValues.ngn_available_amount,
            },
            pending_settlements: {
              USD: balance.dataValues.usd_pending_settlement,
              NGN: balance.dataValues.ngn_pending_settlement,
            },
          },
        })
      );
    } catch (error) {
      next(error);
    }
  }

  static async getPayoutTransactions(req, res, next) {
    try {
      const { status, currency, type, gteAmount, lteAmount } = req.query;
      const { payout_id } = req.params;

      if (!isUUID(payout_id, 4)) {
        throw new APIError(
          "Invalid payout ID",
          HttpStatusCode.BAD_REQUEST,
          "Invalid payout ID"
        );
      }

      let page = req.query.page;

      // Getting valid page number
      page = Number.parseInt(page, 10);
      if (isNaN(page) || page <= 0) {
        page = 1;
      }

      // Constructing query with provided filters
      const query = processFilters(req.user.id, {
        payout_id,
        status,
        currency,
        type,
        gteAmount,
        lteAmount,
      });

      const maxPageSize = 10;
      const { rows, count } = await Transaction.findAndCountAll({
        where: query,
        order: [["createdAt", "DESC"]],
        limit: maxPageSize,
        offset: (page - 1) * maxPageSize,
      });

      const totalPages = Math.ceil(count / maxPageSize);

      res.status(200).json(
        new ResponseDTO(
          true,
          "",
          {
            transactions: rows.map((row) => {
              if (row.type === "card") {
                row.cardDetails.cvv = "***";
              }
              return row;
            }),
          },
          {
            totalTransactions: count,
            page,
            pageSize: rows.length,
            totalPages,
            previousPage:
              page === 1 || totalPages === 0
                ? null
                : page > totalPages
                ? totalPages
                : page - 1,
            nextPage: page < totalPages && totalPages !== 0 ? page + 1 : null,
          }
        )
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = TransactionController;
