const { BaseError } = require("sequelize");
const ResponseDTO = require("../lib/response");
const errorHandler = require("../lib/sequelizeErrorHandler");

const errorRequestHandler = (err, req, res, next) => {
  if (err instanceof SyntaxError && "body" in err) {
    return res.status(400).json(new ResponseDTO(false, "Invalid JSON syntax"));
  }

  if (err.isOperational) {
    return res
      .status(err.httpCode)
      .json(new ResponseDTO(false, err.description));
  }

  if (err instanceof BaseError) {
    const { status, message } = errorHandler(err);
    return res.status(status).json(new ResponseDTO(false, message));
  }

  console.log(err);
  res.status(500).json(new ResponseDTO(false, "Internal Server Error"));
};

const pageNotFoundHandler = (req, res) => {
  res
    .status(404)
    .json(new ResponseDTO(false, `${req.method} ${req.url} not found`));
};

module.exports = { errorRequestHandler, pageNotFoundHandler };
