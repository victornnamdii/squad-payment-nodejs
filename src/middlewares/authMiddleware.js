const { isUUID } = require("validator");
const { APIError } = require("../lib/error");
const { decodeToken } = require("../lib/helpers");
const HttpStatusCode = require("../lib/httpStatusCodes");
const Merchant = require("../models/merchant");

const verifyToken = async (req, res, next) => {
  try {
    const authorization = req.header("Authorization");
    if (
      !authorization ||
      !authorization.startsWith("Bearer ") ||
      authorization.length < 8
    ) {
      throw new APIError(
        "Invalid Authorization",
        HttpStatusCode.UNAUTHORIZED,
        "Invalid Authorization"
      );
    }

    const decodedToken = decodeToken(authorization.slice(7));

    if (isUUID(decodedToken.id, 4)) {
      const merchant = await Merchant.findByPk(decodedToken.id);
      if (!merchant) {
        throw new APIError(
          "Invalid Token",
          HttpStatusCode.FORBIDDEN,
          "Forbidden"
        );
      }

      merchant.password = undefined;
      req.user = merchant;

      return next();
    }

    throw new APIError("Invalid Token", HttpStatusCode.FORBIDDEN, "Forbidden");
  } catch (error) {
    next(error);
  }
};

module.exports = verifyToken;
