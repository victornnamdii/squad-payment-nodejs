const express = require("express");
const merchantRouter = require("./routers/merchant");
const transactionRouter = require("./routers/transaction");
const payoutRouter = require("./routers/payout");
const router = express.Router();

router.route("/").get((req, res, next) => {
  return res.status(200).send({
    success: true,
    message: "It Works",
    data: {},
  });
});

// Merchant routes
router.use("/merchant", merchantRouter);

// Transaction routes
router.use("/transactions", transactionRouter);

// Payout routes
router.use("/payouts", payoutRouter);

module.exports = router;
